import os
from threading import Thread 
from time import sleep
import shlex,subprocess

def run_parallel(stages,filter_size,mHR):
    command = os.system("mkdir data_all/data_{}_{}_filter_{}".format(stages,mHR,filter_size))
    cmd = "opencv_traincascade -data data_all/data_{}_{}_filter_{} -vec positives_series_{}.vec -bg bg.txt -numPos 2000 -numNeg 1000 -numStages {} -w {} -h {} -minHitRate {} -numThreads 4".format(stages,mHR,filter_size,filter_size,stages,filter_size,filter_size,mHR)
    print(cmd)
    actual = subprocess.call(cmd, shell=True)
    return True


if __name__ == "__main__":

	thread7 = Thread(group=None,target=run_parallel, args=(26,24,0.998))
	thread8 = Thread(group=None,target=run_parallel, args=(26,24,0.999))
	thread9 = Thread(group=None,target=run_parallel, args=(27,24,0.995))
	thread10 = Thread(group=None,target=run_parallel, args=(27,24,0.998))
	thread11 = Thread(group=None,target=run_parallel, args=(27,24,0.999))
	thread12 = Thread(group=None,target=run_parallel, args=(28,24,0.995))
	thread13 = Thread(group=None,target=run_parallel, args=(28,24,0.998))
	thread14 = Thread(group=None,target=run_parallel, args=(28,24,0.999))
	thread15 = Thread(group=None,target=run_parallel, args=(22,25,0.995))
	thread16 = Thread(group=None,target=run_parallel, args=(22,25,0.998))
	
	thread7.start()
	thread8.start()
	thread9.start()
	thread10.start()
	thread11.start()
	thread12.start()
	thread13.start()
	thread14.start()
	thread15.start()
	thread16.start()

	thread7.join()
	thread8.join()
	thread9.join()
	thread10.join()
	thread11.join()
	thread12.join()
	thread13.join()
	thread14.join()
	thread15.join()
	thread16.join()


