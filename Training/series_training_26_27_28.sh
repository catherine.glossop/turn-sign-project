#!/bin/bash
for x in 26 27 28 
do
    for i in 22 23 24 25 26 27 28 
    do 
	    for j in 0.995 0.998 0.999
	    do
		    mkdir data_all/data_${i}_${j}_filter_${x}
		    echo "Made directory data_${i}_${j}_filter_${x}"
		    opencv_traincascade -data data_all/data_${i}_${j}_filter_${x} -vec positives_series_${x}.vec -bg bg.txt -numPos 2000 -numNeg 1000 -numStages $i -w ${x} -h ${x} -minHitRate $j -numThreads 4  
	    done
    done
done