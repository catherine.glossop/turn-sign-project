import numpy as np 
import cv2

def is_empty(any_structure):
	if any_structure:
		return False
	else:
		print("Error: Empty tuple")
		return True

turn_sign_cascade = cv2.CascadeClassifier('data_all/data_test/cascade.xml')

img = cv2.imread('test_images/pos_test.png',0)
print(img)

sign = turn_sign_cascade.detectMultiScale(img, 1.05, 3)

for (x,y,w,h) in sign:
	cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

cv2.imshow('img',img)
cv2.waitKey(0)
cv2.destroyAllWindows()


