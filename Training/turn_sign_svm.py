import cv2
import numpy as np 
import os
from extract_data_svm import *

training_data = np.load('svm_data.npy')
training_data = np.float32(training_data)

labels = np.load('labels_data.npy')
labels = np.int64(labels)
labels = np.reshape(labels,(2400,1))


svm = cv2.ml.SVM_create()
svm.setType(cv2.ml.SVM_C_SVC)
svm.setKernel(cv2.ml.SVM_LINEAR)
svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER,100,1e-6))

svm.train(training_data,cv2.ml.ROW_SAMPLE,labels)

directory = "/home/autoronto/turn-sign-project/"

w = 32
h = 32

valid_imgs = open(os.path.join(directory, 'val_com.lst'))
lines = valid_imgs.readlines()
val_set = np.array([],dtype = np.float32)
val_labels = np.array([],dtype = np.float32)
num = 0
for l in lines:
    num += 1
    dims = l.split('.png')[-1] 
    img_path = l.strip(dims)
    list_dim = []
    new_dims = dims.replace('\n','').split(' ')
    for i in new_dims:
        if  i != '':
            list_dim = list_dim + [int(i)]

    x1t = list_dim[1]
    y1t = list_dim[2]
    wt = list_dim[3]
    ht = list_dim[4]
    img = cv2.imread(img_path,0)
    img = img[y1t:y1t+ht,x1t:x1t+wt]

    img = cv2.resize(img,(w,h))
    img = cv2.equalizeHist(img)
    #img = cv2.normalize(img, img, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F)
    val_set = np.append(val_set, img)
    if num <= 120:
    	val_labels = np.append(val_labels, -1)
    else:
    	val_labels = np.append(val_labels, 1)


val_set = np.float32(val_set).reshape(-1,1024)

results = svm.predict(val_set)[1]

tpr = 0
fpr = 0
for i in range(0,len(results)):
    if results[i] == val_labels[i]:
        tpr += 1
    else:
        fpr += 1

precision = float(tpr)/float(fpr + tpr)
recall = float(tpr)/float(results.size)

f1_score = 2 * (precision * recall)/(precision + recall)
print("TPR: {}\nFPR: {}\nPRECISION: {}\nRECALL: {}\nF1 SCORE: {}".format(tpr, fpr, precision, recall, f1_score))




