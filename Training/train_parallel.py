import os
from threading import Thread 
from time import sleep
import shlex,subprocess

def run_parallel(stages,filter_size,mHR):
    command = os.system("mkdir data_all/data_{}_{}_filter_{}".format(stages,mHR,filter_size))
    cmd = "opencv_traincascade -data data_all/data_{}_{}_filter_{} -vec positives_series_{}.vec -bg bg.txt -numPos 2000 -numNeg 1000 -numStages {} -w {} -h {} -minHitRate {} -numThreads 4".format(stages,mHR,filter_size,filter_size,stages,filter_size,filter_size,mHR)
    print(cmd)
    actual = subprocess.call(cmd, shell=True)
    return True


if __name__ == "__main__":
	thread1 = Thread(group=None,target=run_parallel, args=(28,22,0.995))
	thread2 = Thread(group=None,target=run_parallel, args=(28,22,0.998))
	thread3 = Thread(group=None,target=run_parallel, args=(28,22,0.999))
	thread4 = Thread(group=None,target=run_parallel, args=(22,23,0.995))
	thread5 = Thread(group=None,target=run_parallel, args=(22,23,0.998))
	thread6 = Thread(group=None,target=run_parallel, args=(22,23,0.999))
	'''thread7 = Thread(group=None,target=run_parallel, args=(23,23,0.995))
	thread8 = Thread(group=None,target=run_parallel, args=(23,23,0.998))
	thread9 = Thread(group=None,target=run_parallel, args=(23,23,0.999))
	thread10 = Thread(group=None,target=run_parallel, args=(24,23,0.995))
	thread11 = Thread(group=None,target=run_parallel, args=(24,23,0.998))
	thread12 = Thread(group=None,target=run_parallel, args=(24,23,0.999))
	thread13 = Thread(group=None,target=run_parallel, args=(25,23,0.995))
	thread14 = Thread(group=None,target=run_parallel, args=(25,23,0.998))'''
	
	thread1.start()
	thread2.start()
	thread3.start()
	thread4.start()
	thread5.start()
	thread6.start()
	'''thread7.start()
	thread8.start()
	thread9.start()
	thread10.start()
	thread11.start()
	thread12.start()
	thread13.start()
	thread14.start()'''

	thread1.join()
	thread2.join()
	thread3.join()
	thread4.join()
	thread5.join()
	thread6.join()
	'''thread7.join()
	thread8.join()
	thread9.join()
	thread10.join()
	thread11.join()
	thread12.join()
	thread13.join()
	thread14.join()'''


