import os
from threading import Thread 
from time import sleep
import shlex,subprocess

def run_parallel(stages,filter_size,mHR):
    command = os.system("mkdir data_all/data_{}_{}_filter_{}".format(stages,mHR,filter_size))
    cmd = "opencv_traincascade -data data_all/data_{}_{}_filter_{} -vec positives_series_{}.vec -bg bg.txt -numPos 2000 -numNeg 1000 -numStages {} -w {} -h {} -minHitRate {} -numThreads 4".format(stages,mHR,filter_size,filter_size,stages,filter_size,filter_size,mHR)
    print(cmd)
    actual = subprocess.call(cmd, shell=True)
    return True


if __name__ == "__main__":

	thread12 = Thread(group=None,target=run_parallel, args=(24,25,0.998))
	thread13 = Thread(group=None,target=run_parallel, args=(24,25,0.999))
	thread14 = Thread(group=None,target=run_parallel, args=(25,25,0.995))
	thread15 = Thread(group=None,target=run_parallel, args=(25,25,0.998))
	thread16 = Thread(group=None,target=run_parallel, args=(25,25,0.999))
	thread17 = Thread(group=None,target=run_parallel, args=(22,27,0.995))
	thread18 = Thread(group=None,target=run_parallel, args=(22,27,0.998))
	thread19 = Thread(group=None,target=run_parallel, args=(22,27,0.999))
	
	thread12.start()
	thread13.start()
	thread14.start()
	thread15.start()
	thread16.start()
	thread17.start()
	thread18.start()
	thread19.start()


	thread12.join()
	thread13.join()
	thread14.join()
	thread15.join()
	thread16.join()
	thread17.join()
	thread18.join()
	thread19.join()


