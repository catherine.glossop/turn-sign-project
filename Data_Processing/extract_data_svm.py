import cv2
import numpy as np 
import os

training_data = np.array([],dtype = np.float32)
labels = np.array([])
img_cnt = 0

w = 32
h = 32

directory = "/home/autoronto/turn-sign-project/"

valid_imgs = open(os.path.join(directory, 'info_com.lst'))
lines = valid_imgs.readlines()

for l in lines:
    img_cnt += 1
    dims = l.split('.png')[-1]
    img_path = l.strip(dims)
    list_dim = []
    new_dims = dims.replace('\n','').split(' ')
    for i in new_dims:
        if  i != '':
            list_dim = list_dim + [int(i)]
    x1t = list_dim[1]
    y1t = list_dim[2]
    wt = list_dim[3]
    ht = list_dim[4]
    img = cv2.imread(img_path,0)
    img = img[y1t:y1t+ht,x1t:x1t+wt]
    
    img = cv2.resize(img,(w,h))
    img = cv2.equalizeHist(img)
    #norm = 0
    #norm = cv2.normalize(img, norm, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F)

    training_data = np.append(training_data, img)

    if img_cnt <= 1200:
        labels = np.append(labels,-1)
    else:
        labels = np.append(labels,1)

training_data = np.float32(training_data).reshape(-1,1024)


np.save('svm_data',training_data)
np.save('labels_data',labels)

