import pickle
import cv2

data = pickle.load(open('mturk_data.pickle', 'rb'))
imgs_array = pickle.load(open('mturk_imgs.pickle', 'rb'))

folders = ['left_arrow', 'speed_limit_20', 'right_turn_only', 'do_not_enter', 'speed_limit_15', 'speed_limit_10', 'handicap_parking', 'speed_limit_5', 'right_arrow', 'left_turn_only']
info_files = [0, open('speed_limit_20/info.lst', 'w'), 0, 0, open('speed_limit_15/info.lst', 'w'), open('speed_limit_10/info.lst', 'w'), 0, open('speed_limit_5/info.lst', 'w'), 0, 0]
val_files = [0, open('speed_limit_20/val_info.lst', 'w'), 0, 0, open('speed_limit_15/val_info.lst', 'w'), open('speed_limit_10/val_info.lst', 'w'), 0, open('speed_limit_5/val_info.lst', 'w'), 0, 0]
train_count = [0] * 10
val_count = [0] * 10

clahe = cv2.createCLAHE(clipLimit=4.0, tileGridSize=(8,8))


for i in imgs_array:
    info = data.annotations[i][0]
    img_class = int(info['class'])

    if img_class not in [1, 4, 5, 7]:
        continue

    h = round((info['y_max'] - info['y_min']) / 1.5)
    w = round((info['x_max'] - info['x_min']) / 1.5)
    x_min = round(info['x_min'] / 1.5)
    y_min = round(info['y_min'] / 1.5)

    if (img_class == 7 and train_count[img_class] < 1040) or (img_class != 7 and train_count[img_class] < 1200):
        img = cv2.imread(i)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = clahe.apply(gray)
        gray = cv2.resize(gray, (round(gray.shape[1]/1.5), round(gray.shape[0]/1.5)))
        write_path = '{}/training/{}'.format(folders[img_class], i.split('/')[-1])
        path = 'training/{}'.format(i.split('/')[-1])
        cv2.imwrite(write_path, gray)
        info_files[img_class].write(path + '\t1\t{} {} {} {}\n'.format(str(x_min), str(y_min), str(w), str(h)))
        train_count[img_class] += 1
    elif val_count[img_class] < 120:
        img = cv2.imread(i)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = clahe.apply(gray)
        gray = cv2.resize(gray, (round(gray.shape[1]/1.5), round(gray.shape[0]/1.5)))
        write_path = '{}/validation/{}'.format(folders[img_class], i.split('/')[-1])
        path = 'validation/{}'.format(i.split('/')[-1])
        cv2.imwrite(write_path, gray)
        val_files[img_class].write(path + '{} {} {} {}\n'.format(str(x_min), str(y_min), str(w), str(h)))
        val_count[img_class] += 1

    print("Train: " + str(train_count))
    print("Val: " + str(val_count))
    print("----------------------------------------")

    if val_count == [0, 120, 0, 0, 120, 120, 0, 120, 0, 0]:
        break


for f in info_files:
    f.close()

for f in val_files:
    f.close()


print("PROCESS COMPLETED")

