import cv2
import numpy as np
import os
import glob

def process_negatives():
	current = r'/home/autoronto/turn-sign-project/scraping/images/neg_imgs'
	negdir = r'/home/autoronto/turn-sign-project/scraping/images/processedneg'
	window_name = 'image'
	pic_num = 0
	h = 133
	w = 100
	if not os.path.isdir('processedneg'):
		os.mkdir('processedneg')
	for i,file in enumerate(glob.glob('neg_imgs/*.jpg')):
		directory = os.listdir(current)
		try:
			image = cv2.imread(file)
			image_gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
			#image_resized = cv2.resize(image_gray, (2000,2667))
			image_resized = image_gray[5:5+h, 5:5+w]
			cv2.imwrite('./processedneg/grayscale_%03d.jpg'%i,image_resized)

		except Exception as e:
			print(str(e))
	return 0

process_negatives()