import sys 
import cv2
import os
import time

def evaluate(directory, data, num_neighbors=4):
    data_path = directory
    data_path_full = os.path.join(data_path,data)
    print(data_path_full)
    #data_path_full = os.path.join(data_path,data)
    cascade = cv2.CascadeClassifier(os.path.join(data_path_full))
    test = cascade.load(os.path.join(data_path_full))
    # clahe = cv2.createCLAHE(clipLimit=4.0, tileGridSize=(8,8))
    print(os.path.join(directory, 'turn-sign-project/val_com.lst'))
    valid_imgs = open(os.path.join(directory, 'turn-sign-project/val_com.lst'))
    scale = 1.3
    lines = valid_imgs.readlines()
    num_valid = len(lines)
    fpr = 0
    tpr = 0

    for l in lines:
        dims = l.split('.png')[-1]
        img_path = l.strip(dims)
        #x1t = dims.replace('\n','').split(' ')
        list_dim = []
        new_dims = dims.replace('\n','').split(' ')
        for i in new_dims:
        	if  i != '':
        		list_dim = list_dim + [int(i)]
        #print(list_dim)
        x1t = list_dim[1]
        y1t = list_dim[2]
        wt = list_dim[3]
        ht = list_dim[4]
        # print("processing {}".format(img_path))
        gray = cv2.imread(img_path)
        cv2.imshow('img',gray)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        # gray = clahe.apply(gray)
        start = time.time()
        detections = cascade.detectMultiScale(gray, scale, num_neighbors)
        total_time = time.time() - start
        # print("detection time: {} seconds".format(total_time))
        for (x1, y1, w, h) in detections:
            # print("detected: {}\ntrue: {}\n------------------------------".format(str(detections), str([x1t, y1t, wt, ht])))
            if IOU(x1, y1, w, h, x1t, y1t, wt, ht) > 0.5:
                tpr += 1
            else:
                fpr += 1
            

    precision = float(tpr) / float(tpr + fpr)
    recall = float(tpr) / float(num_valid)

    print("TPR: {}\nFPR: {}\nPRECISION: {:.2f}\nRECALL: {:.2f}\nF1 SCORE: {:.2f}".format(tpr, fpr, precision, recall, f1_score(precision, recall)))
    return [tpr,fpr,precision,recall,f1_score(precision, recall)]

def IOU(x1, y1, w, h, x1t, y1t, wt, ht):
    x2 = x1 + w
    y2 = y1 + h
    x2t = x1t + wt
    y2t = y1t + ht

    # Check if the BBs intersect first
    if x2 < x1t or x1 > x2t or y2 < y1t or y1 > y2t:
        return 0.0

    xa = max(x1, x1t)
    ya = max(y1, y1t)
    xb = min(x2, x2t)
    yb = min(y2, y2t)
    intersection = (xb - xa + 1) * (yb - ya + 1)
    area = (w + 1) * (h + 1)
    areat = (wt + 1) * (ht + 1)
    union = area + areat - intersection
    return float(intersection) / float(union)


def f1_score(p, r):
    return 0 if (p + r) == 0 else 2 * (p * r)/(p + r)



'''if __name__ == '__main__':
	if len(sys.argv) == 2:
		evaluate(sys.argv[1])
	else:
		evaluate(sys.argv[1], sys.argv[2])'''
print('Evaluating left arrow')
evaluate('/home/autoronto','merge_ws/src/zeus/lights_n_signs/models/haar_models/leftturnonly_cascade_23.xml',4)

print('Evaluating right arrow')
evaluate('/home/autoronto','merge_ws/src/zeus/lights_n_signs/models/haar_models/rightturnonly_cascade_25.xml',4)


