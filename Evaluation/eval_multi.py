from eval_turn_sign import *
import csv 
import os
lib_cnt = 0
b = open('cascade_results_3.csv','w')
a = csv.writer(b)
data = [['data source','tpr','fpr', 'precision','recall', 'f1_score']]
path = '/mnt/ssd3/catherine/turn-sign-project'
for dir in os.listdir(os.path.join(path,'data_all')):
    filter_list = dir.split('_')
    print(filter_list[4])
    if filter_list[4] == '22':
        lib_cnt += 1
        print(lib_cnt)
        print("Evaluating directory: " + dir)
        row = [dir]+ evaluate(path,dir,2)
        data = data + [row]

a.writerows(data)
b.close()

